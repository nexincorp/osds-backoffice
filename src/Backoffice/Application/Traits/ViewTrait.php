<?php

namespace Osds\Backoffice\Application\Traits;

trait ViewTrait
{

    /**
     *
     * Generates the view with the data provided
     *
     * @param array $data
     *                  items => results
     *                  metadata => info of the model (metadata-model-views-type)
     *
     * @param string $view : force some view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generateView($data = null, $method = null, $context = 'actions')
    {
        #default views of the backoffice
        view()->addLocation(__DIR__ . '/../../theme/templates');
        #custom views for the Project models
        view()->addLocation(base_path() .'/src/theme/templates');
        $model = \Route::current()->parameter('model');

        if($method == null)
        {
            #method is the name of the called function, and the name of the view we are displaying
            $method = debug_backtrace()[1]['function'];
        }

        if(isset($data['twig_vars'])) {
            $twig_vars = $data['twig_vars'];
        } else {
            $twig_vars = [];
        }

        $twig_vars = $this->loadTwigVariables($data, $method, $twig_vars, $model);

        $view = $context . '/';
        $view .= $method;
        if(view()->exists($model.'/'.$method))
        {
            $view .= $model . '/' . $method;
        }

        if($method == 'detail')
        {
            $twig_vars['views']['detail_actions'] = '/twig_partials/detail/actions';
            #check if we have a custom view for the actions of this model
            if(view()->exists($model . '/' . $twig_vars['views']['detail_actions']))
            {
                $twig_vars['views']['detail_actions'] = $model . '/' . $twig_vars['views']['detail_actions'];
            }
        }

        return View($view, $twig_vars);

    }

    private function loadTwigVariables($data, $method, $twig_vars, $model)
    {
        $twig_vars = $this->loadViewDataTwigVariables($twig_vars, $data, $method, $model);
        $twig_vars = $this->loadPreviousSearchesTwigVariables($twig_vars);
        $twig_vars = $this->loadAlertMessages($twig_vars);
        $twig_vars = $this->loadLocales($twig_vars);
        $twig_vars = $this->loadCSSandJS($twig_vars);
        if(
            isset($data['total_items'])
        )
        {
            $twig_vars = $this->loadModelDataTwigVariables($twig_vars, $model);

            if(
                $data['total_items'] > 0
                && $data['total_items'] > count($data['items'])
            )
            {
                $twig_vars = $this->loadPagination($twig_vars, $data['total_items']);
            }
        }

        $twig_vars['backoffice_folder'] = BACKOFFICE_FOLDER;
        $twig_vars['config'] = $this->config;

        return $twig_vars;
    }

    private function loadViewDataTwigVariables($twig_vars, $data, $method, $model)
    {
        #models for navigation
        $twig_vars['models_list'] = $this->models;
        $twig_vars['model'] = $model;
        #page title and secion
        $twig_vars['action'] = $method;

        #data itself to show
        $twig_vars['data'] = isset($data['items'])?$data['items']:null;
        $twig_vars['total_items'] = isset($data['total_items'])?$data['total_items']:null;
        $twig_vars['schema'] = isset($data['schema'])?$data['schema']:null;
        $twig_vars['editable_referenced_models_contents'] = isset($data['required_models_contents'])?$data['required_models_contents']:null;

        #templates for tinymce

        if(function_exists('get_server_id'))
        {
            $server_id = get_server_id();
            $twig_vars['theme_blocks_json'] = $this->getTemplateJSForTinyMce($server_id);
            $twig_vars['theme_style_sheet'] = '/styles/' . $server_id . '.css';
        }

        #data passed on url
        $twig_vars['GET'] = $this->request_data['get'];

        return $twig_vars;
    }

    private function loadPreviousSearchesTwigVariables($twig_vars)
    {
        if(!empty($this->request_data['get']) && !empty($this->request_data['get']['search_fields']))
        {
            $twig_vars['search_fields'] = $this->request_data['get']['search_fields'];
            $twig_vars['query_string_search_fields'] = http_build_query(['search_fields' => $this->request_data['get']['search_fields']]);
        }
        return $twig_vars;
    }

    private function loadModelDataTwigVariables($twig_vars, $model)
    {
        #it is not possible to call it on constructor, Route::current (on getmetadata command) is null
        $metadata = $this->loadModelMetadata($model);
        $twig_vars['models_metadata'] = $metadata['items'][0];
        return $twig_vars;
    }

    private function loadAlertMessages($twig_vars)
    {
        $twig_vars['alert_message'] = $this->getAlertMessages();
        return $twig_vars;
    }

    public function loadPagination($twig_vars, $total) {

        $vars['mode'] = (isset($this->config['domain_structure']['pagination']['display_mode']))?$this->config['domain_structure']['pagination']['display_mode']:'pages';

        $items_per_page = (isset($this->config['domain_structure']['pagination']['items_per_page']))?$this->config['domain_structure']['pagination']['items_per_page']:10;
        $pages_per_page = (isset($this->config['domain_structure']['pagination']['pages_per_page']))?$this->config['domain_structure']['pagination']['pages_per_page']:10;

        preg_match('/query_filters\[page\]=(.*)\/?/i', $_SERVER['REQUEST_URI'], $page_num);
        if(isset($page_num[1])) {
            $current_page = $page_num[1];
            $href = str_replace('query_filters[page]='.$current_page, 'query_filters[page]=%page%', $_SERVER['REQUEST_URI'] );
        } else {
            $current_page = 1;
            $href = $_SERVER['REQUEST_URI'];
            if(strstr($_SERVER['REQUEST_URI'], '?'))
            {
                $href .= '&';
            } else {
                $href .= '?';
            }
            $href .= 'query_filters[page]=%page%';
        }

        $num_pages = ceil($total / $items_per_page);

        if($num_pages <= 1)
        {
            return $twig_vars;
        }

        $first_page = max(
            1,
            $current_page - floor($pages_per_page / 2)
        );

        $last_page = min(
            $num_pages,
            $first_page + $pages_per_page - 1
        );

        if($first_page != 1) {
            $first_page_link = str_replace('%page%', 1, $href);
            $vars['first'] = $first_page_link;
        }

        if($current_page != 1) {
            $prev_page_link = str_replace('%page%', $current_page - 1, $href);
            $vars['previous'] = $prev_page_link;
        }

        for($i=$first_page;$i<=$last_page;$i++) {
            if($i == $current_page) {
                $vars['current_page'] = $i;
            }
            $page_link = str_replace('%page%', $i, $href);
            $vars['pages'][$i] = $page_link;
        }

        if($current_page < $num_pages) {
            $next_page_link = str_replace('%page%', $current_page + 1, $href);
            $vars['next'] = $next_page_link;
        }

        if($last_page != $num_pages) {
            $last_page_link = str_replace('%page%', $num_pages, $href);
            $vars['last'] = $last_page_link;
        }


        $twig_vars['paginator'] = $vars;
        $twig_vars['items_per_page'] = $items_per_page;


        return $twig_vars;

    }

    private function loadCSSandJS($twig_vars)
    {
        $twig_vars['css_file_contents'] = file_get_contents($this->vendor_path . 'theme/css/styles.css');
        $twig_vars['js_file_contents'] = file_get_contents($this->vendor_path . '/theme/js/scripts.js');
        return $twig_vars;
    }

    private function loadLocales($twig_vars)
    {
        $locale = $this->loadLocalization($this->vendor_path . '/Localization/');

        $twig_vars['locale'] = $locale;
        return $twig_vars;
    }

    private function getTemplateJSForTinyMce($server_id)
    {
        $blocks_path = base_path('vendor/osds/dynamic-site/src/themes/common/templates/blocks/');
        $theme_blocks = file_get_contents($blocks_path . 'definitions.json');
        $theme_blocks_array = json_decode($theme_blocks, true);
        foreach($theme_blocks_array as &$theme_block)
        {
            if(isset($theme_block['url']))
            {
                $theme_block['content'] = file_get_contents($blocks_path . $theme_block['url']);
                unset($theme_block['url']);
            }
        }

        return json_encode($theme_blocks_array);
    }

    private function loadModelMetadata($model)
    {
        try
        {
            $this->requestRelatedModels($model);
            return $this->performAction('getmetadata');
        } catch(\Exception $e)
        {
            dd($e->getMessage());
        }
    }

    private function requestRelatedModels($model)
    {
        if(isset($this->models[$model]['related_models'])) {
            $this->request_data['get']['related_models'] = implode(',', $this->models[$model]['related_models']);
        }
    }
}