<?php


namespace Osds\Backoffice\Application\Controllers;

use Illuminate\Http\Request;
use Log;

class BackofficeController extends BaseController {

    var $user = null;

    var $models_metadata = null;


    public function index(Request $request)
    {
        $model = array_keys($this->config['domain_structure']['models'])[0];
        $this->redirect("/{$model}");
    }

}