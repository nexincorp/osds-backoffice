<?php

namespace Osds\Backoffice\Application\Controllers;

class LoginController extends BaseController
{

    const var_session_name = 'backoffice_user_logged';

    var $request_data;

    public function login()
    {
        return $this->generateView(null, 'login', 'session');
    }

    public function postLogin()
    {
        $this->request_data['get'] = [
            'search_fields' => [
                'email' => $this->request_data['post']['email']
            ]
        ];
        $data = $this->performAction('list', 'user');

        if(
            isset($data)
            && $data['total_items'] == 1
            && password_verify($this->request_data['post']['password'], $data['items'][0]['password'])
        )
        {
            $this->session->put(self::var_session_name, $data['items'][0]);
            $this->redirect('/');
        } else {
            $this->redirect(self::pages['session']['login'], 'danger', 'login_ko');
        }
    }

    public static function checkAuth($session)
    {

        $backoffice_token = $session->get(self::var_session_name);

        if(!(
            $backoffice_token != null
            && self::isValidToken($backoffice_token)
        ))
        {
            return false;
        }

        return true;
    }

    public static function isValidToken($backoffice_token)
    {
        return true;
    }

    public function logout()
    {
        $this->session->remove(self::var_session_name);
        $this->redirect(self::pages['session']['login']);
    }

}